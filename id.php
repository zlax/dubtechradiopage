<?php
// Shoutcast
function getIDshout($radioip,$radioport,$postfix = "GET /7.html HTTP/1.1\nUser-Agent:Mozilla\n\n") {
    $open = fsockopen($radioip,$radioport,$errno,$errstr,'.5');
    if ($open) {
        fputs($open,$postfix);
        stream_set_timeout($open,'1');
        $read = fread($open,255);
        $exploded = explode(",",$read);
        if ($exploded[6] == '' || $exploded[6] == '</body></html>') {
            $text = 'streaming'; } else { $text = $exploded[6]; }
        $id = str_replace("</body></html>","",$text);
    } else { return false; }
    fclose($open);
    return $id;
}
// Icecast
// https://stackoverflow.com/a/17109654
function getIDice($streamingUrl, $interval, $offset = 0, $headers = true) {
    $needle = 'StreamTitle=';
    $ua = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.110 Safari/537.36';
    $opts = [
        'http' => [
            'method' => 'GET',
            'header' => 'Icy-MetaData: 1',
            'user_agent' => $ua
        ]
    ];
    if (($headers = get_headers($streamingUrl)))
        foreach ($headers as $h)
            if (strpos(strtolower($h), 'icy-metaint') !== false && ($interval = explode(':', $h)[1]))
                break;
    $context = stream_context_create($opts);
    if ($stream = fopen($streamingUrl, 'r', false, $context)) {
        $buffer = stream_get_contents($stream, $interval, $offset);
        fclose($stream);
        if (strpos($buffer, $needle) !== false) {
            $title = explode($needle, $buffer)[1];
            return substr($title, 1, strpos($title, ';') - 2);
        } else
            return getMp3StreamTitle($streamingUrl, $interval, $offset + $interval, false);
    } else
        throw new Exception("Unable to open stream [{$streamingUrl}]");
}
// Anima Amoris
function getIDanima($amoris) {
    $url = "http://amoris.sknt.ru/".$amoris."/stats.json";
    $headers = array('User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $data = curl_exec($ch);
    curl_close($ch);
    $data = json_decode($data, true);
    if (!empty($data)) $data = $data["songtitle"];
    if (substr($data, -7) == "sknt.ru") $data = stristr($data, " * anima.sknt.ru", true);
    return $data;
}

// first
echo "id:;:";
try {
    echo getIDanima("dubtechno"); // Anima Amoris [DubTech]
} catch (Exception $exfirst) {
    echo "unavailable";
}
// second
echo ":;:";
try {
    echo getIDanima("dubtechnomix"); // Anima Amoris [DubTech Mix]
} catch (Exception $exsecond) {
    echo "unavailable";
}
// third
echo ":;:";
try {
    echo getIDshout("79.120.39.202","9009"); // Radio Caprice - Dub Techno
} catch (Exception $exthird) {
    echo "unavailable";
}
// fourth
echo ":;:";
try {
    echo getIDice("http://185.61.124.104:8066/live", 19200); // Loca FM Dub Techno
} catch (Exception $exfourth) {
    echo "unavailable";
}
// fifth
echo ":;:";
try {
    $fifthIDice = getIDice("http://94.130.113.214:8000/dubtechno", 19200); // Schizoid Dub Techno
    if (bin2hex($fifthIDice) == "fffe66202d20fffe52") echo "online streaming";
    else echo $fifthIDice;
} catch (Exception $exfifth) {
    echo "unavailable";
}
// sixth
echo ":;:";
try {
    echo getIDice("http://78.47.31.164:8002/dub", 19200); // MABU Beatz Dub Techno
} catch (Exception $exsixth) {
    echo "unavailable";
}
// seventh
echo ":;:";
try {
    echo getIDice("http://195.133.48.93:8000/MixCult_96kbps", 19200); // MixCult Deep Techno Radio
} catch (Exception $exseventh) {
    echo "unavailable";
}
// eighth
echo ":;:";
try {
    echo getIDice("https://c2.radioboss.fm:18382/stream", 19200); // dubte.ch
} catch (Exception $exeighth) {
    echo "unavailable";
}
// ninth
echo ":;:";
try {
    echo getIDice("http://prem2.di.fm:80/dubtechno_hi?266a3dc03aaa30a0807d9062", 19200); // Digitally Imported Dub Techno
} catch (Exception $exninth) {
    echo "unavailable";
}
// tenth
echo ":;:";
try {
    echo getIDanima("minimal"); // Anima Amoris [MinimalDeepTech]
} catch (Exception $extenth) {
    echo "unavailable";
}
echo ":;:";

?>
