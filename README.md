dub techno shoutcast and icecast streams

http://dub.tech.soundragon.su

under DWTWL 2.55 license

***

Radio stations you are trying to listen to are running on an unencrypted port (not 443). This is quite common and it's not technically necessary for radio stations to be encrypted but nowadays web browsers like Chrome have started disallowing unencrypted content to be shown in an otherwise encrypted websites.

Solution: click on the padlock icon to the left of the URL box and then from the drop down menu select the 'Site Settings' option, you can then scroll down to the 'Insecure content' option at the bottom of the page and select 'Allow', this will create an exception for the website and allow you to continue using the player as before.

Source: https://support.google.com/chrome/thread/29505473?hl=en&msgid=29673696
